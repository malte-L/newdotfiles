#! /bin/bash
a=$(i3-msg -t get_workspaces | /usr/local/bin/jq-linux64 '.[] | "\(.name)"' | sed 's|"||g' | dmenu)
i3-msg workspace $a

